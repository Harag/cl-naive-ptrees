(defsystem "cl-naive-ptrees"
  :description "Functions to make it easier to work with plist(s) and plist trees. Works with plist(s) pairs as units and not as individual list items."
  :version "2021.6.25"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ()
  :components (
	       (:file "src/package")
	       (:file "src/naive-ptrees" :depends-on ("src/package"))))

