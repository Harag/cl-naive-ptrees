(defparameter *plist*
  '((:attribute "x"
     :attribute-1 "eish"
     :children ((:attribute "y"
		 :children ((:attribute "a")
			    (:attribute "b")
			    (:attribute "c"))))
     :other-children ((:attribute "xa")
		      (:attribute "xa")
		      (:attribute "xa")))

    (:attribute "z"
     :children ((:attribute "e")
		(:attribute "f")
		(:attribute "g"
		 :grand-children ((:attribute "ga")
				  (:attribute "gb")
				  (:attribute "gc")
				  (:attribute "x" :attribute-1 "eish")))))
    (:attribute "x" :children ((:attribute "duh")))
    (:attribute "x" :children nil)
    (:attribute "x" :attribute-z (:attribute "?"))))

;; Traverse the tree touching each pair

(let ((pairs))
  (traverse *plist*
	    (lambda (attribute parent)
	      (declare (ignore parent))
	      (push attribute pairs)))
  (reverse pairs))

;; Traverse the tree touching all attributes = (:attribute "x")

(let ((pairs))
  (traverse-path '((:attribute "x"))
		 *plist*
		 (lambda (attribute parent)
		   (declare (ignore parent))
		   (push attribute pairs)))
  (reverse pairs))

;; Traverse the tree touching all attributes that have a key of
;; :children and a pair (:attribute "x")

(let ((pairs))
  (traverse-path '((:attribute "x") (:children))
		 *plist*
		 (lambda (attribute parent)
		   (declare (ignore parent))
		   (push attribute pairs)))
  (reverse pairs))

;; Traverse the tree touching all pairs that have a key of :children

(let ((pairs))
  (traverse-path '((:children))
		 *plist*
		 (lambda (attribute parent)
		   (declare (ignore parent))
		   (push attribute pairs)))
  (reverse pairs))

;; Traverse the tree and produce a value duplicate of the full tree
(map-plist *plist*
	   (lambda (attribute parent)
	     (declare (ignore parent))
	     attribute))

;; Traverse the tree and produce a value duplicate of the full tree
(map-path '((:attribute "x")) *plist*
	  (lambda (attribute parent)
	    (declare (ignore parent))
	    attribute))

;; Traverse the tree and produce changed tree, adding attribute
;; to the value of existing attributes for the path.

;; Doing it your self
(map-path '((:attribute "x") (:children))
	  *plist*
	  (lambda (attribute parent)
	    (declare (ignore parent))
	    (list (first attribute)
		  (if (second attribute)
		      (append (second attribute) '(:attribute-x "NEW"))
		      (list '(:attribute-x "NEW"))))))

;; Using the append-attribute-value utility

;; Updates all instance of path
(append-attribute-value '((:attribute "x") (:children)) *plist* '(:attribute-x "NEW"))

;; Updates only the first instance of path
(append-attribute-value '((:attribute "x") (:children)) *plist* '(:attribute-x "NEW") :nth 0)

(append-attribute-value '((:attribute-1 "eish") (:children)) *plist* '(:attribute-x "NEW"))

(append-attribute-value '((:children)) *plist* '(:attribute-x "NEW"))

;; Traverse the tree and produce changed tree, adding an attribute to
;; a plist for the path

;; Doing it yourself
(map-path '((:attribute "x"))
	  *plist*
	  (lambda (attribute parent)
	    (declare (ignore parent))
	    (append attribute (list :attribute-x "NEW"))))

;; Using the append-attribute utility
;; Updates all instance of path
(append-attribute '((:attribute "x")) *plist* (list :attribute-x "NEW"))

;; Updates only the first instance of path
(append-attribute '((:attribute "x")) *plist* (list :attribute-x "NEW") :nth 0)

(append-attribute '((:attribute-1 "eish")) *plist* (list :attribute-x "NEW"))

;; Traverse the tree and produce changed tree, changing the value of
;; attributes that match the path

;; Doing it yourself
(map-path '((:attribute "x"))
	  *plist*
	  (lambda (attribute parent)
	    (declare (ignore parent))
	    (list (first attribute) "NEW")))

;; Using the update-attribute utility
;; Updates all instance of path
(update-attribute '((:attribute "x")) *plist* "NEW")

;; Updates only the first instance of path
(update-attribute '((:attribute "x")) *plist* "NEW" :nth 0)

(update-attribute '((:attribute-1 "eish")) *plist* "NEW")


;; Traverse the tree and produce changed tree, removing attributes that match the path

;; Doing it yourself
(map-path '((:attribute "x"))
	  *plist*
	  (lambda (attribute parent)
	    (declare (ignore parent attribute))
	    nil))

;;Doing it using the remove-attribute utility
;; Updates all instance of path
(remove-attribute '((:attribute "x")) *plist*)
;; Updates only the first instance of path
(remove-attribute '((:attribute "x")) *plist* :nth 0)

(remove-attribute '((:attribute-1 "eish")) *plist*)
